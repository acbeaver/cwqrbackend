var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dataRouter = require('./routes/data');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/cw',dataRouter);

global.transactions = {};

global.statusCodes={
  "CONTINUE":100,
  "PROCESSING":102,
  "OK":200,
  "CREATED":201,
  "ACCEPTED":202,
  "NO_CONTENT":204,
  "MOVED_PERMANENTLY":301,
  "MOVED_TEMPORARILY":302,
  "BAD_REQUEST":400,
  "UNAUTHORIZED":401,
  "FORBIDDEN":403,
  "NOT_FOUND":404,
  "METHOD_NOT_ALLOWED":405,
  "INTERNAL_SERVER_ERROR":500
}
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
