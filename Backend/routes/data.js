var QRCode = require('qrcode')

var express = require('express');
var router = express.Router();
var request = require('request');
const fs = require('fs');
var base64Img = require('base64-img');

function GenericResponse(status, title, comment, data){
    this.status = status;
    this.title = title;
    this.comment = comment;
    this.data = data;
}

var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
        console.log('content-type:', res.headers['content-type']);
        console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    });
};


router.post('/make/:transactionId', function(req,res,next){
    transactions[req.params.transactionId] = {};

    res.status(statusCodes.CREATED);
    res.send(new GenericResponse(statusCodes.CREATED, "Created New Transaction","via REST API",req.params.transactionId));
});
router.post('/post/:transactionId', function(req,res,next){
    transactions[req.params.transactionId] = req.body;


    fs.writeFile(`./dest/${req.params.transactionId}.json`, JSON.stringify(req.body), (err) => {
        // throws an error, you could also catch it here
        if (err) throw err;

        // success case, the file was saved
        console.log('Lyric saved!');
    });
    res.status(statusCodes.OK);
    res.send(new GenericResponse(statusCodes.OK, "Updated Transaction","via REST API",transactions));

});

router.get('/get/:transactionID', function(req, res, next){
    if(transactions[req.params.transactionID] != undefined && transactions[req.params.transactionID] != null){
        console.log(transactions[req.params.transactionID] )
    QRCode.toDataURL(JSON.stringify(transactions[req.params.transactionID]) , function (err, url) {
        if (err != undefined && err != null) {
            //url = encodeURI(url);
            //res.redirect(url);
            //res.setHeader('content-type', 'image/png');

            //res.attachment(`${url}`);


            base64Img.img(url, 'dest', `CryptoWerkHash-${req.params.transactionID}`, function (err, filepath) {
                if(err){
                    res.status(statusCodes.INTERNAL_SERVER_ERROR);
                    res.send(new GenericResponse(statusCodes.INTERNAL_SERVER_ERROR,"Error recieved during QR generation",err,{}));
                }else{
                    res.download(filepath, `CryptoWerkHash-${req.params.transactionID}`);

                }
            });


        } else {
            res.status(statusCodes.INTERNAL_SERVER_ERROR);
            res.send(new GenericResponse(statusCodes.INTERNAL_SERVER_ERROR, "Error generating QR Code", err, {}))
        }
    });}
    else{
        res.status(statusCodes.INTERNAL_SERVER_ERROR);
        res.send(new GenericResponse(statusCodes.INTERNAL_SERVER_ERROR, "Data field does not exist", req.params.transactionID,{}));

        }



});


module.exports = router;
